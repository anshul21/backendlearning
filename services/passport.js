
var passport = require("passport");
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const Keys = require('../confiq/Keys')
const mongoose = require('mongoose');

const User = require('../models/User')
passport.use(
    new GoogleStrategy({
        clientID: Keys.googleClientID,
    clientSecret: Keys.googleClientSecret,
    callbackURL: '/auth/google/callback',
},(accessToken,refreshToken,profile,done) => {
   new User({googleId: profile.id}).save()
   .then(console.log("updated"))
   .catch(
       function(err)
       {console.log(err)})

}
)
);